numpy
scipy
pandas
scikit-learn
markdown
matplotlib
gdal
xlrd
xlwt
openpyxl
# desactivation mlpy (pb. dépendances)
#https://sourceforge.net/projects/mlpy/files/mlpy%203.5.0/mlpy-3.5.0.tar.gz/download
pytest
#pytest-cov
#pytest-pep8
#pytest-cache
seaborn
# necessary to web application
django>=1.8.1,<=1.8.99
