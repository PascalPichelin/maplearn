# -*- coding: utf-8 -*-
"""
Installateur de Mapping Learning
Created on Thu Oct 13 10:48:51 2016

@author: thomas_a
"""
from __future__ import print_function

import inspect
import sys
import os
import setuptools
from setuptools.command.test import test as TestCommand
from distutils.command.install_data import install_data

class PyTest(TestCommand):
    """
    Class to run unit tests with the installer
    """
    test_package_name = 'maplearn'

    def finalize_options(self):
        TestCommand.finalize_options(self)
        _test_args = [
            # uncomment to assert pep8
            # '--pep8',
            '--ignore=env',
        ]
        extra_args = os.environ.get('PYTEST_EXTRA_ARGS')
        if extra_args is not None:
            _test_args.extend(extra_args.split())
        self.test_args = _test_args
        self.test_suite = True

    def run_tests(self):
        import pytest
        errno = pytest.main(self.test_args)
        sys.exit(errno)

def get_ori(path):
    """
    Get list of original configuration files to edit when installing maplearn

    Arg:
        * path (str): path to look for original configuration files

    Returns:
        list: list of original configuration files
    """
    return [os.path.join(path, i) for i in os.listdir(path) \
            if os.path.splitext(i)[-1] == '.ORI']

def update_cfg_file(fichier, prefix):
    """ edite les fichiers de configuration pour les rendre utilisables sur le
    poste => PATCH à supprimer dés que possible"""

    with open(fichier) as __file:
        with open(fichier[:-4], 'w') as __file_out:
            for line in __file:
                line = line.format(path=prefix)
                __file_out.write(line)
    print("Configuration file %s updated" % fichier)

# application path
dir_app = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))

try:
    reqs = open(os.path.join(dir_app, 'requirements.txt')).read()
except (IOError, OSError):
    print('Dependencies not met')
dir_pack = os.path.join(dir_app, 'maplearn')

examples_path = os.path.join(dir_app, "examples")
for file_path in get_ori(examples_path):
    update_cfg_file(file_path, dir_app)
for file_path in get_ori(dir_pack):
    update_cfg_file(file_path, dir_app)

# create tmp directory (for unit tests...)
if not os.path.exists(os.path.join(dir_app, 'tmp')):
    os.mkdir(os.path.join(dir_app, 'tmp'))

setuptools.setup(
    name='maplearn',
    version='0.3.1',
    test_suite="test",
    packages=setuptools.find_packages(exclude=['test']),
    description='Mapping Learning',
    url='https://bitbucket.org/thomas_a/maplearn/',
    author="Alban Thomas",
    license='LGPL',
    install_requires=reqs,
    cmdclass={'test': PyTest, 'install_data': install_data},
    data_files=[('maplearn', [os.path.join(dir_pack, 'logging.cfg')]),
                (os.path.join('maplearn', 'app', 'style'),
                 [os.path.join(dir_pack, 'app', 'style', 'style.css')]),
                (os.path.join('maplearn', 'app', 'img'),
                 [os.path.join(dir_pack, 'app', 'img', i + '.png') for i in \
                 ['logo', 'info', 'warning', 'error']])],
)
