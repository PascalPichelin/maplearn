# Mapping Learning #
![logo_100.png](https://bitbucket.org/repo/5Rn74B/images/2433912438-logo_100.png)


Mapping Learning se veut un outil pour assister l'utilisateur dans une tache de cartographie automatisée (d'images issues de télédétection, notamment). Il donne accès à un grand nombre d'algorithmes issus du domaine du *Machine Learning*, ainsi que des moyens pour les appliquer à des jeux de données de différentes natures (tableau, image, données vectorielles).


## Fonctionnalités ##

* Nombreux algorithmes de classification supervisée et non supervisée, et de régression
* Emploi de méthode robuste (k-fold) et possibilité d'optimiser les algorithmes
* Plusieurs prétraitements : réductions de dimensions...
* Lecture/Ecriture d'une grande variété de formats de fichiers, géographiques ou non
* Rendu du résultat sous la forme d'un rapport standardisé
* Accompagnement de l'utilisateur avec des conseils statistiques ou empiriques

## Développement ##

Mapping Learning est développé en Python et fait appel à des librairies Open Source dont scikit-learn et mlpy pour les algorithmes de Machine Learning, osgeo (Gdal/Ogr) pour la manipulation des données géographiques.

Mapping Learning est un logiciel libre, sous licence LGPL.
Alban Thomas (alban.thomas@univ-rennes2.fr)

![letg.jpeg](https://bitbucket.org/repo/5Rn74B/images/1800694338-letg.jpeg)![sgdn.jpeg](https://bitbucket.org/repo/5Rn74B/images/612087003-sgdn.jpeg)![ur2.png](https://bitbucket.org/repo/5Rn74B/images/2154561398-ur2.png)