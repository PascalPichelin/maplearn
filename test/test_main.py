# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 22:00:54 2015

@author: thomas_a
"""
from __future__ import print_function

import os
import unittest
from shutil import rmtree

import numpy as np

from test import DIR_APP, DIR_DATA, DIR_TMP
from maplearn.filehandler.imagegeo import ImageGeo
from maplearn.filehandler.shapefile import Shapefile
from maplearn.app.main import Main
from maplearn.app.config import Config

class TestMain(unittest.TestCase):
    """ Tests unitaires de la classe Clustering (classif non supervisée)
    """

    def setUp(self):
        for f in os.listdir(DIR_TMP):
            f = os.path.join(DIR_TMP, f)
            try:
                os.remove(f)
            except OSError:
                rmtree(f)                

        src_data = os.path.join(DIR_DATA, 'landsat_rennes.tif')
        src_samples = os.path.join(DIR_DATA, 'samples_landsat_rennes.tif')
        self.benImg = Main(DIR_TMP, type='classification',
                                algorithm='knn')
        self.benImg.load(src_samples)
        self.benImg.load_data(src_data)

    def test_load_image(self):
        """
        Test de chargement des échantillons et des données à partir d'images
        """
        self.assertGreater(self.benImg.dataset.X.shape[0], 0)
        self.assertEqual(self.benImg.dataset.X.shape[0],
                         self.benImg.dataset.Y.shape[0])

    def test_classification(self):
        """
        Apply classification on a known dataset
        """
        ben = Main(DIR_TMP, type='classification', algorithm='knn')
        ben.load('iris')
        ben.preprocess()
        try:
            ben.process(False)
        except Exception as e:
            self.fail(e)

    def test_clustering(self):
        """
        Apply clustering on a known dataset
        """
        ben = Main(DIR_TMP, type='clustering', algorithm='mkmeans')
        ben.load('iris')
        ben.preprocess()
        try:
            ben.process(False)
        except Exception as e:
            self.fail(e)
        finally:
            ben = None

    def test_regression(self):
        """
        Apply regression on a known dataset
        """
        ben = Main(DIR_TMP, type='regression', algorithm='lm')
        ben.load('boston')
        ben.preprocess()
        try:
            ben.process(False)
        except Exception as e:
            self.fail(e)
        finally:
            ben = None

    def test_classify_reducted(self):
        """
        Apply reduction to IRIS dataset and the classify
        """
        ben = Main(DIR_TMP, type='classification', algorithm='knn')
        ben.load('iris')
        ben.preprocess(reduction='lda')
        try:
            ben.process(False)
        except Exception as e:
            self.fail(e)
        finally:
            ben = None

    def test_classify_csv_image(self):
        """
        Test d'application d'une classification à une image, avec des
        echantillons provenant d'un fichier csv
        """
        src_data = os.path.join(DIR_DATA, 'landsat_rennes.tif')
        src_samples = os.path.join(DIR_DATA, 'echantillon_rennes.csv')
        output = os.path.join(DIR_TMP, 'export_csv_img')
        ml = Main(output, type='classification',
                                algorithm=['knn','nearestc', 'lda'])
        ml.load(src_samples, label='classe', label_id='classe_id')
        ml.load_data(src_data)

        ml.preprocess()
        ml.process(True)
        ml = None
        self.assertTrue(os.path.join(output, 'result.tif'))

    def test_classify_image(self):
        """
        Test d'application d'une classification à une image
        """
        out_file = os.path.join(DIR_TMP, 'export.tif')
        self.benImg.preprocess()
        self.benImg.process(True)
        self.assertTrue(os.path.exists(out_file))
        img = ImageGeo(out_file)
        img.read()
        # vérifie que l'image est bien une image avec des entiers, avec le
        # nombre de classes attendues
        self.assertLessEqual(len(np.unique(img.data)), 4)

    def test_classify_shp(self):
        """
        Test d'application d'une classification à un shapefile
        """
        out_file = os.path.join(DIR_TMP, 'export.shp')
        benShp = Main(DIR_TMP, type='classification',
                                algorithm='knn')
        benShp.load(os.path.join(DIR_DATA, 'echantillon.shp'),
                         features=None, label='ECH')
        benShp.load_data(os.path.join(DIR_DATA, 'data.shp'))
        benShp.preprocess()
        benShp.process(True)
        benShp = True
        
        # check that a shapefile containing result has been created
        self.assertTrue(os.path.exists(out_file))
        shp = Shapefile(out_file)
        shp.read()
        # check the content of the output shapefile
        self.assertLessEqual(len(np.unique(shp.data)), 3)

    def test_with_config(self):
        """
        Run main using config class
        """
        __cfg = Config(os.path.join(DIR_APP, 'examples', 'example5.cfg'))
        __ml = Main(__cfg.io['output'], codes=__cfg.codes, **__cfg.process)
        __params = {i: __cfg.io[i] for i in ('label', 'label_id', 'features',
                    'na')}
        #TODO: bug dans pipeline mentionnant "Id_classes" dans ligne ci-dessous
        __ml.load(__cfg.io['samples'], **__params)
        __ml.load_data(__cfg.io['data'], features=__cfg.io['features'])
        __ml.preprocess(**__cfg.preprocess)
        __ml.process(optimize=__cfg.process['optimize'],
                   predict=__cfg.process['predict'])
        self.assertTrue(os.path.exists(os.path.join(__cfg.io['output'],
                                                    'export.shp')))

if __name__ == '__main__':
    unittest.main()
