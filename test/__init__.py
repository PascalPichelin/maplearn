# -*- coding: utf-8 -*-
"""
Mapping Learning : Unitary tests
Created on Tue Aug 16 18:37:27 2016

@author: thomas_a
"""
import os
import inspect
from shutil import rmtree

def empty_folder(path):
    """
    Removes files and sub-folders in a given path
    """
    for f in os.listdir(DIR_TMP):
        f = os.path.join(DIR_TMP, f)
        try:
            os.remove(f)
        except OSError:
            rmtree(f)

# application path
DIR_APP = os.path.dirname(os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe()))))
DIR_DATA = os.path.join(DIR_APP, 'datasets')
DIR_TMP = os.path.join(DIR_APP, 'tmp')

if not os.path.exists(DIR_TMP):
    os.mkdir(DIR_TMP)
