# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 22:00:54 2015

@author: thomas_a
"""
from __future__ import print_function

import os
import unittest

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

from test import DIR_APP
from maplearn.app.config import Config


class TestConfig(unittest.TestCase):
    """ Tests unitaires de la classe Config
    """

    def setUp(self):
        self._cfg = Config(os.path.join(DIR_APP, 'examples', 'example1.cfg'))
        self._cfg.read()

    def test_missing_file(self):
        """ chargement d'une configuration à partir d'un fichier absent"""
        self.assertRaises(configparser.Error, Config, 'missing_file.cfg')

    def test_check(self):
        """
        Vérifie la configuration du fichier de configuration actuelle
        """
        self.assertEqual(self._cfg.check(), 0)

    def test_check2(self):
        """
        Vérifie la configuration d'un fichier de configuration
        """
        __cfg = Config(os.path.join(DIR_APP, 'examples', 'example5.cfg'))
        self.assertEqual(__cfg.check(), 0)

    def test_get_legend(self):
        """
        Obtenir une legende (code/libelle des classes) à partir d'un fichier
        de configuration (avec une nomenclature contenant 7 codes)
        """
        __cfg = Config(os.path.join(DIR_APP, 'examples', 'example2.cfg'))
        self.assertEqual(__cfg.check(), 0)
        self.assertEqual(len(__cfg.codes), 7)

    def test_write(self):
        """
        vérifie ecriture fichier de config
        """
        __cfg = Config(os.path.join(DIR_APP, 'examples', 'example1.cfg'))
        fichier = os.path.join(DIR_APP, 'tmp', 'cfg_write1.cfg')
        __cfg.write(fichier)
        self.assertTrue(os.path.exists(fichier))

    def test_write_legend(self):
        """
        Obtenir une legende (code/libelle des classes) à partir d'un fichier
        de configuration (avec une nomenclature contenant 7 codes)
        """
        __cfg_in = Config(os.path.join(DIR_APP, 'examples', 'example2.cfg'))
        fichier = os.path.join(DIR_APP, 'tmp', 'cfg_write2.cfg')
        __cfg_in.write(fichier)
        __cfg_out = Config(fichier)
        self.assertEqual(len(__cfg_out.codes), 7)

    def test_set_parameter(self):
        """
        Loads configuration and then update several of them
        """
        for i in ['scale', 'balance', 'separability']:
            self._cfg.preprocess[i] = False
            self.assertFalse(self._cfg.preprocess[i])
            self._cfg.preprocess[i] = True
            self.assertTrue(self._cfg.preprocess[i])
    
    def test_set_features(self):
        """
        Loads configuration and then update list of features
        """
        print(self._cfg.io['features'])
        self._cfg.io['features'] = 'a'
        self._cfg.read()
        test = isinstance(self._cfg.io['features'], list) and \
               len(self._cfg.io['features']) == 1
        print(self._cfg.io['features'])
        self.assertTrue(test)
        self._cfg.io['features'] = 'a,b'
        self._cfg.read()
        test = isinstance(self._cfg.io['features'], list) and \
               len(self._cfg.io['features']) == 2
        self.assertTrue(test)        
        self._cfg.io['features'] = ['a', 'b', 'c']
        self._cfg.read()
        test = isinstance(self._cfg.io['features'], list) and \
               len(self._cfg.io['features']) == 3
        self.assertTrue(test)
        print(self._cfg.io['features'])

if __name__ == '__main__':
    unittest.main()
