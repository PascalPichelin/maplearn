# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:24:50 2016

@author: thomas_a
"""
import unittest
import numpy as np

from maplearn.ml.distance import Distance


class TestDistance(unittest.TestCase):
    """ Tests unitaires de la classe Distance
    """

    def setUp(self):
        self.__data = {'a': np.random.random(50),
                       'b': np.random.random(50),
                       'c': np.random.random(50), }
        self.dist = Distance()

    def test_greaterzero(self):
        """
        Une distance doit être >= 0
        """
        d1 = self.dist.run(x=self.__data['a'], y=self.__data['b'])
        self.assertGreaterEqual(d1, 0)

    def test_symetrie(self):
        """
        Symétrie d'une distance : d(a,b) == d(b,a)
        """
        d1 = self.dist.run(x=self.__data['a'], y=self.__data['b'])
        d2 = self.dist.run(x=self.__data['b'], y=self.__data['a'])
        self.assertEqual(d1, d2)

    def test_inegalite_triangulaire(self):
        """
        Inegalité triangulaire : d(a,c) <= d(a,b)+d(b,c)
        """
        d1 = self.dist.run(x=self.__data['a'], y=self.__data['c'])
        d2 = self.dist.run(x=self.__data['a'], y=self.__data['b'])
        d2 += self.dist.run(x=self.__data['b'], y=self.__data['c'])
        self.assertLessEqual(d1, d2)

if __name__ == '__main__':
    unittest.main()
