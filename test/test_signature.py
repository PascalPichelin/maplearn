# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:17:10 2016

@author: thomas_a
"""
import os
import unittest

from test import DIR_TMP, empty_folder
from maplearn.datahandler.loader import Loader
from maplearn.datahandler.signature import Signature


class TestSignature(unittest.TestCase):
    """ Tests unitaires de la classe Signature
    """

    def setUp(self):
        loader = Loader('iris')
        self.__data = loader.X
        self.__features = ['Sepal length', 'Sepal width', 'Petal length',
                           'Petal width']
        empty_folder(DIR_TMP)

    def test_boxplot(self):
        """
        Graphique Boxplot : vérifie que tout se passe bien
        """
        out_file = os.path.join(DIR_TMP, 'boxplot.png')
        sig = Signature(self.__data, features=self.__features)
        sig.plot(out_file=out_file)
        self.assertTrue(os.path.exists(out_file))

    def test_plot(self):
        """
        plot : vérifie que tout se passe bien
        """
        out_file = os.path.join(DIR_TMP, 'plot.png')
        sig = Signature(self.__data, features=self.__features)
        sig.plot(out_file=out_file)
        self.assertTrue(os.path.exists(out_file))

if __name__ == '__main__':
    unittest.main()
