# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:14:49 2016

@author: thomas_a
"""
import unittest
import os
import random
import numpy as np

from maplearn.filehandler.imagegeo import ImageGeo
from test import DIR_DATA, DIR_TMP


class TestImageGeo(unittest.TestCase):
    """
    Tests unitaires autour des images géographiques
    """

    def test_read(self):
        """
        Test de lecture
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        img.read_geo()
        test = img.data.shape[0] == 674 and img.data.shape[1] == 561
        self.assertTrue(test)

    def test_copy(self):
        """
        Test de recopie d'une image
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        img.read_geo()
        shape = img.data.shape
        img.write(os.path.join(DIR_TMP, 'landsat_rennes.tif'))
        img = None
        img = ImageGeo(os.path.join(DIR_TMP, 'landsat_rennes.tif'))
        img.read_geo()
        self.assertEquals(img.dims, shape)

    def test_coord(self):
        """
        Test de conversion de coordonnees pixels <=> geographique
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        img.read_geo()
        for _ in range(50):
            i = random.randint(0, img.data.shape[1])
            j = random.randint(0, img.data.shape[0])
            lon, lat = img.pixel2xy(j, i)
            self.assertEquals((j, i), img.xy2pixel(lon, lat))

    def test_coord_ul(self):
        """
        Test de conversion de coordonnees pixels <=> geographique, du coin
        haut-gauche
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        img.read_geo()
        i = j = 0
        lon, lat = img.pixel2xy(j, i)
        self.assertEquals((j, i), img.xy2pixel(lon, lat))

    def test_coord_lr(self):
        """
        Test de conversion de coordonnees pixels <=> geographique, du coin
        bas à droite
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        img.read_geo()
        j, i = img.data.shape[0], img.data.shape[1]
        lon, lat = img.pixel2xy(j, i)
        self.assertEquals((j, i), img.xy2pixel(lon, lat))

    def test_img_data(self):
        """
        Reformatte 1 image en jeux de données puis le retour
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        img.read_geo()
        data_img = img.data_2_img(img.img_2_data())

        self.assertTrue((img.data == data_img).all())

    def test_export_classif(self):
        """
        Exporte une classification (matrice de nombres entiers)
        en utilisant la géographie d'une image source
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        out_file = os.path.join(DIR_TMP, 'test_export_classif.tif')
        img.read_geo()
        classif = np.random.randint(1, 10, size=img.dims).astype(np.int8)
        img.write(out_file, data=classif, depth='uint8')
        img = ImageGeo(out_file)
        img.read_geo()
        result = img.data
        print(result.dtype)
        self.assertTrue(result.dtype == 'uint8')

if __name__ == '__main__':
    unittest.main()
