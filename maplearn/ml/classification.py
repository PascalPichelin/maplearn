# -*- coding: utf-8 -*-
"""
Supervised classification

Supervised classification methods are used to generate a map with each pixel
assigned to a class based on its multispectral composition. The classes are
determined based on the spectral composition of training areas defined by the
user.

Classification is supervised and need samples for fitting. The output will be
be a matrix with integer values.


Example:
    >>> from maplearn.datahandler.loader import Loader
    >>> from maplearn.datahandler.packdata import PackData
    >>> loader = Loader('iris')
    >>> data = PackData(X=loader.X, Y=loader.Y, data=loader.aData)
    >>> lst_algos = ['knn', 'lda', 'rforest']
    >>> dir_out = os.path.join('maplean_path', 'tmp')
    >>> clf = Classification(data=data, dirOut=dir_out, algorithms=lst_algos)
    >>> clf.run()
"""
from __future__ import print_function

import sys
import os
import logging
import re

import numpy as np
import pandas as pd
import pylab as pl
from sklearn.cross_validation import StratifiedKFold, cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn import neighbors, linear_model, svm, tree, neural_network
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier
#from sklearn.manifold.isomap import Isomap
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.linear_model import SGDClassifier
from sklearn.semi_supervised import LabelPropagation, LabelSpreading
from sklearn.linear_model import Perceptron, PassiveAggressiveClassifier
from sklearn.feature_selection import RFECV

from maplearn.ml.machine import Machine
from maplearn.ml.confusion import Confusion
from maplearn.ml.distance import Distance
from maplearn.app.reporting import str_table, str_synthesis

logger = logging.getLogger(__name__)


def lcs_kernel(x, y):
    """
    Custom kernel based on LCS (Longest Common Substring)

    Args:
        * x and y (matrices)

    Returns:
        matrix of float values
    """
    dist = Distance(x, y)
    return dist.run(meth='lcs')

def svm_kernel(x, y):
    """
    Custom Kernel based on DTW

    Args:
        * x and y (matrices)

    Returns:
        matrix of float values
    """
    dist = Distance(x, y)
    return dist.run(meth='dtw')

# LISTE DES ALGOS EN STOCK
TUNE_ALGO = dict(
    knn={'n_neighbors': [3, 5, 10, 20],
         'weights': ['uniform', 'distance'],
         'algorithm': ['auto', 'ball_tree', 'kd_tree'],
         'leaf_size': [15, 30, 50]},
    log={'penalty': ['l1', 'l2'], 'C': [0.1, 1, 10, 100]},
    svm=[{'kernel': ['rbf'], 'gamma': np.logspace(-6, -1, 6),
          'C': [0.1, 1, 10, 100, 1000]},
         {'kernel': ['linear', 'poly', 'sigmoid'],
          'C': [0.1, 1, 10, 100, 1000]}],
    nearestc={'shrink_threshold': [None, 0.1, 0.2, 0.5]},
    lda=[{'solver': ['lsqr', 'eigen'],
          'shrinkage':[None, 0.1, 0.2, 0.5, 'auto']},
         {'solver': ['svd'], 'tol':[None, 0.1, 0.2, 0.5]}])

def skreport_md(report):
    """
    Convert a classification report given by scikit-learn into a markdown table
    
    Arg:
        * report (str): classification report
    
    Returns:
        str: a table formatted as markdown
    """
    test_s = report.split('\n')
    for i in [0, 0, -1, -2]:
        test_s.pop(i)
    for i, v in enumerate(test_s):
        test_s[i] = re.findall( r'\d+\.*\d*', v)
    test_s[-1].insert(0, 'avg/total')
    dct_report = dict()
    header = ['class', 'Accuracy', 'recall', 'f1-score', 'support']
    for i, v in enumerate(header):
        dct_report[v] = [s[i] for s in test_s]

    return str_table(header, 50, **dct_report)


class Classification(Machine):
    """
    Apply supervised classification onto a dataset:

    * samples needed for fitting
    * data to predict

    Args:
        * data (PackData): data to play with
        * algorithms (list or str): name of an algorithm or list of algorithms
        * **kwargs: other parameters like kfold
    """
    __SCORING = "precision_weighted"

    def __init__(self, data, algorithms=None, **kwargs):
        """
        Initialiaze
        """
        Machine.ALL_ALGOS = dict(
            knn=neighbors.KNeighborsClassifier(),
            log=linear_model.LogisticRegression(),
            tree=tree.DecisionTreeClassifier(),
            gnb=GaussianNB(),
            mnb=MultinomialNB(),
            lda=LinearDiscriminantAnalysis(),
            nearestc=neighbors.NearestCentroid(),
            svm=svm.SVC(kernel='rbf'),
            svc=svm.SVC(kernel='linear'),
            gboost=GradientBoostingClassifier(),
            bag=BaggingClassifier(),
            rforest=RandomForestClassifier(),
            extra=ExtraTreesClassifier(),
            sgd=SGDClassifier(),
            propag=LabelPropagation(),
            spreading=LabelSpreading(),
            ada=AdaBoostClassifier(),
            perceptron=Perceptron(),
            passive=PassiveAggressiveClassifier(),
            mlp=neural_network.MLPClassifier(),
            )
        # Paramètre de la Cross-Validation
        self.kfold = 3 if 'kfold' not in kwargs else kwargs['kfold']
        self.__dct_scores = dict(Algorithm=[], Accuracy=[], Recall=[], F1=[],
                                 Kappa=[])
        Machine.__init__(self, algorithms=algorithms, data=data, **kwargs)

        self.algorithms = algorithms

        if 'metric' in kwargs:
            if kwargs['metric'] == 'dtw':
                dist = Distance()
                metric = dist.dtw
                kernel = svm_kernel
            elif kwargs['metric'] == 'lcs':
                dist = Distance()
                metric = dist.lcs
                kernel = lcs_kernel
            else:
                metric = kwargs['metric']
                # TODO : avoir un kernel propre a chaque distance
                kernel = 'linear'
            for clf in self._algorithms:
                if 'metric' in self._algorithms[clf].get_params():
                    self._algorithms[clf].set_params(metric=metric)
                elif 'kernel' in self._algorithms[clf].get_params():
                    self._algorithms[clf].set_params(kernel=kernel)

    def load(self, data):
        """
        Loads necessary data for supervised classification:

        * samples (X and Y): necessary for fitting
        * other data to predict

        Args:
            * data (PackData)
        """
        Machine.load(self, data)
        if self._data.Y is None:
            raise AttributeError('No samples available')
        if self._data.data is None:
            self._data.data = self._data.X

        # analyse de l'echantillon en fonction de la cross-validation
        frq = self._data.classes
        if min(frq.values()) < self.kfold:
            print("""WARNING : les classes suivantes ne contiennent pas
                  assez d'echantillons :""")
            for k, v in frq:
                i = 0
                if v < self.kfold:
                    self._data.X = self._data.X[self._data.Y != k, :]
                    self._data.Y = self._data.Y[self._data.Y != k]
                    sys.stdout.write("%i (%i) | " % (k, v))
                    i += 1
            print("\n=> %i classes seront ignorees" % i)
            logger.warning('%i classes ignored due to too few members', i)
        return 0

    def predict_1(self, algo, proba=False):
        """
        Apply a classifier on data to predict

        Args:
            * algo (str): name of the algorithme to apply
            * proba (bool): should probabilities be added to result
        """
        Machine.predict_1(self, algo)
        if proba:
            try:
                self.algo.set_params(probability=True)
            except ValueError:
                logger.warning('Probabilities unavailable with %s', algo)
                proba = False
        # classification sur tous les donnees
        self.algo.fit(self._data.X, self._data.Y)
        logger.info('Algorithm %s trained on whole dataset', algo)
        result = self.algo.predict(self._data.data).astype(np.int8)
        logger.info('Prediction on whole dataset with %s', algo)
        # 1er resultat ou ajout d'une classification a d'autres resultats
        # existants
        if self.result is None:
            self.result = pd.DataFrame(data=result, columns=[algo, ],
                                       dtype=np.int8)
        else:
            self.result[algo] = result
        if proba:
            # recupere la proba max (celle de la classe retenue)
            try:
                self.result[algo + '_pr'] = \
                    np.round(np.max(self.algo.predict_proba(
                             self._data.data).astype(np.float), 1), 5)
            except (NotImplementedError, AttributeError):
                logger.warning('Probabilities unavailable with %s', algo)
            else:
                logger.info('Probabilities using %s calculated', algo)

    def fit_1(self, algo):
        """
        Fits a classifier using cross-validation

        Arg:
            * algo (str): name of the classifier
        """
        Machine.fit_1(self, algo)
        # 1. CLASSIFICATION par k-fold
        print('\n####%s - Entrainement classification####' % algo)
        skf = StratifiedKFold(y=self._data.Y, n_folds=self.kfold)
        i = 0
        k = np.zeros(shape=self.kfold)
        for t, v in skf:
            print('\n#####%s (%i/%i)#####' % (algo, i + 1, self.kfold))
            # entrainement sur un k-fold
            try:
                self.algo.fit(self._data.X[t, :], self._data.Y[t])
            except ValueError as e:
                logger.error('%s ne peut être appliqué', algo)
                logger.error(e.message)
                return None
            except (AttributeError, TypeError):
                logger.error('ERROR with %s classifier => Exclusion', algo)
                # self._algorithms.pop(name) # exclusion de la classification
                return None

            # prediction sur le reste
            result_clf = self.algo.predict(self._data.X[v, :])
            mat_confusion = Confusion(self._data.Y[v], result_clf)
            mat_confusion.calcul_matrice()
            str_file = os.path.join(self.dir_out,
                                    '_'.join((algo, str(i), 'cm')))
            mat_confusion.export(fTxt=str_file + '.txt', fPlot=str_file + '.png')
            k[i] = mat_confusion.kappa
            i += 1
            
            # Underfitting/Overfitting : scores of classification 
            # training vs. test
            print('\n**Score (F1) :**')
            print('* Entrainement = %.2f' % self.algo.score(self._data.X[t, :],
                                                           self._data.Y[t]))
            print('* Validation = %.2f' % self.algo.score(self._data.X[v, :],
                                                         self._data.Y[v]))
            print("*NB: Un score eleve sur les echantillons d'entrainement et \
                    faibles pour ceux de la validation -> signe de \
                    surentrainement*")

            # Classification report
            str_report = str(classification_report(self._data.Y[v], result_clf))
            print(skreport_md(str_report))        
            print('\n\n<div style="clear:both;">\n</div>\n')
        # STATSTIQUES de la Classification (k-fold)
        try:
            scores = cross_val_score(self.algo, self._data.X, self._data.Y,
                                     cv=self.kfold, scoring=self.__SCORING)
            recalls = cross_val_score(self.algo, self._data.X, self._data.Y,
                                     cv=self.kfold, scoring='recall_weighted')
            f1s = cross_val_score(self.algo, self._data.X, self._data.Y,
                         cv=self.kfold, scoring='f1_weighted')
        except ValueError:
            logger.warning('%s - Cross-validation score not computable',
                           algo)
        else:
            self._fitted = True
            self.__dct_scores['Algorithm'].append(algo)
            self.__dct_scores['Accuracy'].append(str_synthesis(scores))
            self.__dct_scores['Recall'].append(str_synthesis(recalls))
            self.__dct_scores['F1'].append(str_synthesis(f1s))
            self.__dct_scores['Kappa'].append(str_synthesis(k))
            print('\n**%s - Cross-Validation %i-fold :**' % (algo, self.kfold))
            print("* Accuracy: %s" % self.__dct_scores['Accuracy'][-1])
            print("* Kappa : %s\n" % self.__dct_scores['Kappa'][-1])

    def export_tree(self, out_file=None):
        """
        Exports a decision tree

        Args:
            * out_file (str): path to the output file
        """
        if out_file is None or out_file == '':
            out_file = os.path.join(self.dir_out, 'decision_tree.dot')
        clf = tree.DecisionTreeClassifier()
        clf.fit(X=self._data.X, y=self._data.Y)
        with open(out_file, 'w') as file_:
            file_ = tree.export_graphviz(clf, out_file=file_)
        logger.info("Decision tree saved to %s", out_file)

    def run(self, predict=False):
        """
        Applies every classifiers specified in 'algorithms' property

        Args:
            * predict (bool): should be the classifier only fitted or also used
                              to predict?
        """
        Machine.run(self, predict)
        print('*Cross-Validation %i-fold*\n' % self.kfold)
        size = 20
        header = ['Algorithm', 'Accuracy', 'Recall', 'F1', 'Kappa']
        print(str_table(header=header, size=size, **self.__dct_scores))

        lst_exclu = list(set(self.algorithms) ^ set(self.__dct_scores['Algorithm']))
        if len(lst_exclu) > 0:
            print("\n+ %i classifications avec erreurs : %s"
                  % (len(lst_exclu), ','.join(lst_exclu)))

    def rfe(self, algo):
        """
        Recursive Feature Elimination: selects features based on a classifier

        Args:
            * algo (str): name of the classifier to use
        """
        if algo is not None:
            self.algo = self._algorithms[algo]
        if self.algo is None:
            algo = self.algorithms[0]
            self.algo = self._algorithms[algo]
        # Create the RFE object and compute a cross-validated score.
        rfecv = RFECV(estimator=self.algo, step=1,
                      cv=StratifiedKFold(self._data.Y, self.kfold),
                      scoring='accuracy')
        try:
            rfecv.fit(self._data.X, self._data.Y)
        except ValueError:
            logger.warning("Unable to use %s classifier to select features",
                           self._algorithms[algo])
            return 1
        print("Optimal number of features : %d" % rfecv.n_features_)

        # Plot number of features VS. cross-validation scores
        pl.figure()
        pl.xlabel("Number of features selected")
        pl.ylabel("Cross validation score (nb of misclassifications)")
        pl.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)
        pl.show()

        # affichage textuel des features retenus
        feat = rfecv.get_support(True)
        # application de la sélection des features
        self._data.X = self._data.X[:, feat]

        feat = [str(i) for i in feat]
        print('%i Features retenus : %s' % (len(feat), ' | '.join(feat)))

    def optimize(self, algo):
        """
        Optimize parameters of a classifier

        Args:
            * algo (str): name of the classifier to use
        """
        if algo not in TUNE_ALGO:
            logger.warning('No available optimization available for %s', algo)
            return None

        # normalisation des donnees => accelere GridSearchCV
        if np.mean(self._data.X) < 0.9 or np.mean(self._data.X) > 1.1:
            self._data.scale()
        score_name = self.__SCORING.split('_')[0]
        print("####Optimisation de %s (but : best %s)####\n"
              % (algo, score_name))
        gs = GridSearchCV(self._algorithms[algo],
             param_grid=TUNE_ALGO[algo],
             n_jobs=4, cv=self.kfold, scoring=self.__SCORING)

        gs.fit(self._data.X, self._data.Y)
        y_true, y_pred = self._data.Y, gs.predict(self._data.X)

        print("\n * Meilleurs parametres : " + str(gs.best_params_))
        print("\n * Meilleur estimateur: " + str(gs.best_estimator_))
        print("\n * Meilleur score : %.3f \n" % gs.best_score_)
        print(classification_report(y_true, y_pred))

        self._algorithms[algo + '.optim'] = gs.best_estimator_
