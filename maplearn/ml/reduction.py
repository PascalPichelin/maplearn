# -*- coding: utf-8 -*-
"""
Reduction of dimensions


The number of dimensions are reduced by selecting some of the features (like in
kbest approach) or transforming them (like in PCA...). This reduction is
applied to samples and the data to predict in further step.

Several approaches are available, which are listed in the class attribute
"ALG_ALGOS".


"""
from __future__ import print_function
import logging
import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import feature_selection
from sklearn import decomposition
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.svm import NuSVR

from maplearn.app.reporting import str_table
from maplearn.ml.machine import Machine

logger = logging.getLogger('maplearn.' + __name__)
estimator = NuSVR(kernel="linear", cache_size=2000, max_iter=2000)


class Reduction(Machine):
    """
    This class reduces the number of dimensions by selecting some of the features
    or transforming them (like in PCA...). This reduction is applied to samples
    and the data to predict in further step.

    Args:
        * data (PackData): dataset to reduced
        * algorithms (list): list of algorithm(s) to apply on dataset
        * **kwargs: parameters about the reduction (numberof components) or the
                    dataset (like features)

    Attributes:
        * attributes inherited from Machine classe
        * ncomp (int): number of components expected
    """

    def __init__(self, data, algorithms=None, **kwargs):

        Machine.ALL_ALGOS = dict(
            pca=decomposition.PCA(),
            kbest=feature_selection.SelectKBest(feature_selection.f_classif),
            lda=LinearDiscriminantAnalysis(),
            rfe=feature_selection.RFE(estimator=estimator, step=1),
            kernel_pca=decomposition.KernelPCA(kernel="rbf",
                                               fit_inverse_transform=True))

        self.ncomp = 1  # nombre de dimensions souhaitees
        if 'features' in kwargs:
            self.__features = kwargs['features']
        else:
            self.__features = None

        Machine.__init__(self, algorithms=algorithms, data=data, **kwargs)
        if 'ncomp' in kwargs:
            self.__nb_comp(kwargs['ncomp'])

        self.algorithms = algorithms
        self.result = dict()

    def load(self, data):
        """
        Loads dataset to reduce

        Args:
            * data (PackData): dataset to load
        """
        Machine.load(self, data)
        if self._data.Y is None:
            logger.warning('No samples specified')
        if self._data.data is None:
            self._data.data = self._data.X
        self.__check()

        if self.__features is None:
            if self._data.X is not None:
                last_col = self._data.X.shape[1] + 1
            else:
                last_col = self._data.data.shape[1] + 1
            features = range(1, last_col)
            self.__features = [str(i) for i in features]

    def __check(self):
        """
        Chekc if the dataset is compatible with reduction
        """
        if not isinstance(self._data.data, np.ndarray):
            raise TypeError("Data is expected to be an array")

        elif self._data.data.ndim != 2:
            raise IndexError("Dataset should be 2D (%i dimensions found"
                             % self._data.data.ndim)
        elif self._data.data.shape[1] < self.ncomp:
            str_msg = "Dataset does not contain enough features (%i <= %i)" \
                      % (self._data.data.shape[1], self.ncomp)
            logger.critical(str_msg)
            raise IndexError(str_msg)
        else:
            logger.debug('Dataset ready to be reduced')

        if self._data.X is None or self._data.Y is None:
            logger.warning("X ou Y non renseigné => seront ignorés")
            self._data.X = None
            self._data.Y = None

        if self._data.X is not None:
            if self._data.X.ndim != self._data.data.ndim:
                raise IndexError("X and data have different number of \
                                 dimensions: %i vs. %i", self._data.X.ndim,
                                 self._data.data.ndim)
            elif self._data.X.shape[1] != self._data.data.shape[1]:
                raise IndexError("X & data have different number of features:\
                                 %i vs. %i", self._data.X.shape[1],
                                 self._data.data.shape[1])
        if self._data.Y is not None:
            if self._data.Y.ndim != 1:
                raise TypeError("Y should be a vector (containing labels")
            elif self._data.Y.shape[0] != self._data.X.shape[0]:
                raise IndexError("Y and X have different number of \
                                 individuals: %i vs. %i",
                                 self._data.Y.shape[0],
                                 self._data.X.shape[0])
    def __plot_pca(self):
        """
        Plots barplots
        """
        logger.debug('Creating chart')
        sns.set(style="whitegrid")
        # Initialize the matplotlib figure
        f, ax = plt.subplots(figsize=(5, 4))
        # Draw a count plot to show the number of planets discovered each year
        index=['pca'+ str(i+1) for i in range(self.ncomp)]
        data = pd.Series(dict(variance=self.algo.explained_variance_ratio_,
                         features=index))
        sns.barplot(y="variance", x='features', data=data, palette="GnBu_d")
        __file = os.path.join(self.dir_out, 'reduction_pca.png')
        f.savefig(__file)
        print('\n\n![%s](%s){: .sig}\n\n' % ("sign", __file))

    def __nb_comp(self, n_feat):
        """
        Guess the number of dimensions that could be expected after reduction,
        if the user does not mention it.

        Args:
            * n_feat (int or None): number of features expected or None if it
                                    should be guessed

        Returns:
            int: the number of features expected
        """
        if n_feat is not None:
            self.ncomp = n_feat
        elif self._data.Y is not None:
            ncomp = len(np.unique(self._data.Y)) - 1  # nb. de dimensions a conserver
            if ncomp < self.ncomp:
                ncomp = self.ncomp  # au moins nMin dimensions
        else:
            logger.warning('No information about number of components')

        # application de l'objectif [nombre de dimensions]
        if self.ncomp is not None:
            logger.info('Purpose : reducing to %i dimension(s)', self.ncomp)
            for i in ('k', 'n_features_to_select', 'n_components'):
                Machine._params[i] = self.ncomp
        return self.ncomp

    def fit_1(self, algo):
        """
        Fits one reduction algorithm to the dataset

        Args:
            * algo (str): name of the algorithm to fit
        """
        Machine.fit_1(self, algo)
        self.result['data'] = None
        self.result['X'] = None

        # algorithms that need samples to fit
        lst_algos = ('kbest', 'lda', 'rfe', 'kernel_pca')
        if self._data.X is None or self._data.Y is None:
            if algo in lst_algos:
                str_msg = "%s Reduction needs samples" % algo
                logger.error(str_msg)
                return None

        # fitting
        if algo in lst_algos:
            try:
                self.algo.fit(self._data.X, self._data.Y)
            except ValueError as error:
                logger.critical(error.message)
                raise ValueError(error.message)
        elif algo == 'pca':
            self.algo.fit(self._data.data)
        self._fitted = True

    def predict_1(self, algo):
        """
        Applies chosen way of reduction to the dataset

        Args:
            algo (str): name of the algorithm to apply
        """
        Machine.predict_1(self, algo)
        print('\n#### Reduction a %i dimension(s)####' % self.ncomp)

        if algo in ('lda', 'pca', 'kernel_pca'):
            if self._data.X is not None:
                self.result['X'] = self.algo.transform(self._data.X)
            self.result['data'] = self.algo.transform(self._data.data)
            self.__features = [algo + str(i+1) for i in range(self.ncomp)]
        elif algo in ('rfe', 'kbest'):
            # selection des features a conserver
            self.__features = [self.__features[c] for c in
                               self.algo.get_support(True)]
            self.result['data'] = self._data.data[:, self.algo.get_support(True)]
            self.result['X'] = self._data.X[:, self.algo.get_support(True)]
        str_msg = 'Dimensions reduites de %i => %i' \
                  % (self._data.data.shape[1], self.result['data'].shape[1])
        logger.info(str_msg)
        self.__print(algo)
        print("\n" + str_msg)

    def run(self, predict=True, ncomp=None):
        """
        Executes reduction of dimensions (fits and applies)

        Args:
            * predict (bool): should apply the reduction or just fit the
                              algorithm ?
            * ncomp (int): number of dimensions expected

        Returns:
            * array: reduced features data
            * array: reduced samples features
            * list: liste of features
        """
        if ncomp is not None:
            self.__nb_comp(ncomp)
        Machine.run(self, predict)
        return(self.result['data'], self.result['X'], self.__features)

    def __print(self, algo):
        """
        Textual display of the resulting reduction

        Args:
            * algo (str): name of the algorithm applied
        """
        if algo == 'rfe':
            ranks = list(self.algo.ranking_)
            columns = [x for (_, x) in sorted(zip(ranks, self.__features))]
            ranks.sort()
            columns = columns[:self.algo.n_features_]
            ranks = ranks[:self.algo.n_features_]
            print(str_table(header=['Features', 'Ranks'],
                            Ranks=[str(r) for r in ranks],
                            Features=columns))
        elif algo == 'kbest':
            # tri par scores descendant
            scores = [self.algo.scores_[r] for r in self.algo.get_support(True)]
            pvalues = [self.algo.pvalues_[p] for p in
                       self.algo.get_support(True)]
            columns = [x for (_, x) in sorted(zip(scores, self.__features),
                                              reverse=True)]
            pvalues = [x for (_, x) in sorted(zip(scores, pvalues),
                                              reverse=True)]
            scores.sort(reverse=True)
            print(str_table(header=['Features', 'Score', 'Pvalue'],
                            Features=self.__features,
                            Score=["%.2f" % s for s in scores],
                            Pvalue=["%.2g" % s for s in pvalues]))

        elif algo == 'pca':
            self.__plot_pca()
            print(str_table(header=['Composante', 'Variance'],
                            Composante=self.__features,
                            Variance=["%.1f%%" % (p * 100) for p in
                                      self.algo.explained_variance_ratio_]))
            
