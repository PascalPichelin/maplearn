# -*- coding: utf-8 -*-
"""
Clustering: unsupervised classification

A clustering algorithm groups the given samples, each represented as a vector x
in the N-dimensional feature space, into a set of clusters according to their
spatial distribution in the N-D space. Clustering is an unsupervised
classification as no a priori knowledge (such as samples of known classes) is
assumed to be available.

Clustering is unsupervised and does not need samples for fitting. The output
will be a matrix with integer values.

Example:
    >>> from maplearn.datahandler.loader import Loader
    >>> from maplearn.datahandler.packdata import PackData
    >>> loader = Loader('iris')
    >>> data = PackData(X=loader.X, Y=loader.Y, data=loader.aData)
    >>> lst_algos = ['knn', 'lda', 'rforest']
    >>> dir_out = os.path.join('maplean_path', 'tmp')
    >>> cls = Clustering(data=data, dirOut=dir_out, algorithms='mkmeans')
    >>> cls.run()
"""

from __future__ import print_function
import logging

import numpy as np
import pandas as pd

from sklearn import cluster
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

from maplearn.ml.machine import Machine

logger = logging.getLogger('maplearn.' + __name__)


class Clustering(Machine):
    '''
    Apply one or several methods of clustering onto a dataset

    Args:
        * data (PackData): dataset to play with
        * algorithms (str or list): name of algorithm(s) to use
        * **kwargs: more parameters about clustering. The 'metric' to use, the
                    number of clusters expected ('n_clusters')
    '''

    def __init__(self, data, algorithms=None, **kwargs):
        if 'metric' in kwargs:
            Machine._params['metric'] = kwargs['metric']
        else:
            Machine._params['metric'] = 'euclidean'
        # n_clusters = nombre de clusters attendus
        if 'n_clusters' in kwargs:
            self.n_cluster = {'f': kwargs['n_clusters']}
            Machine._params['n_clusters'] = kwargs['n_clusters']
        else:
            self.n_cluster = {'f': 15}
        logger.info('%i clusters wanted', self.n_cluster['f'])
        # LISTE DES ALGOS EN STOCK
        Machine.ALL_ALGOS = dict(
            mkmeans=cluster.MiniBatchKMeans(),
            kmeans=cluster.KMeans(n_jobs=-1),
            ms=cluster.MeanShift(bin_seeding=True),
            ward=cluster.AgglomerativeClustering(linkage='ward'),
            spectral=cluster.SpectralClustering(affinity='rbf', eigen_tol=0.25,
                                                eigen_solver='arpack',
                                                assign_labels="kmeans"),
            dbscan=cluster.DBSCAN(),
            propa=cluster.AffinityPropagation(damping=0.8, preference=None), )
        Machine.__init__(self, data, algorithms=algorithms, **kwargs)
        self.algorithms = algorithms
        self.__x = None

    def load(self, data):
        """
        Loads necessary data for clustering: no samples are needed.

        Arg:
            * data (PackData): data to play with
        """
        Machine.load(self, data)
        if self._data.data is None:
            self._data.data = self._data.X
        # matrice de distances
        self._data.data = StandardScaler().fit_transform(self._data.data)
        return 0

    def fit_1(self, algo):
        """
        Fits one clustering algorithm

        Arg:
            * algo (str): name of the algorithm to fit
        """
        Machine.fit_1(self, algo)
        print('\n####%s - Entrainement clustering####' % algo)

        self.__x = self._data.data
        try:
            self.algo.fit(self.__x)
        except Exception as ex:
            logger.error(ex)
        else:
            self._fitted = True

    def predict_1(self, algo, export=False):
        """
        Makes clustering prediction using one algorithme

        Args:
            * algo (str): name of the algorithm to use
            * export (bool): should the result be exported?
        """
        Machine.predict_1(self, algo)
        if hasattr(self.algo, 'labels_'):
            result = self.algo.labels_.astype(np.int)
        elif hasattr(self.algo, 'predict'):
            result = self.algo.predict(self.__x)
        else:
            result = self.algo.fit_predict(self.__x)
        n_clusters = np.unique(result).size
        print('%i Clusters expected - Got %i clusters'
              % (self.n_cluster['f'], n_clusters))
        if hasattr(self.algo, 'score'):
            print('* Score : %.2f' % self.algo.score(self.__x))
        try:
            score = metrics.silhouette_score(self.__x, result,
                                             metric=Machine._params['metric'])
        except MemoryError:
            logger.error('Not enough memory to compute score for %s', algo)
        else:
            print('* Silhouette Score : %.2f' % score)

        score = metrics.calinski_harabaz_score(self.__x, result)
        print('* Calinski and Harabaz score : %.2f' % score)

        # 1er resultat ou ajout d'une classification a d'autres resultats
        # existants
        if self.result is None:
            self.result = pd.DataFrame(data=result, columns=[algo, ],
                                       dtype=np.float16)
        else:
            self.result[algo] = result
