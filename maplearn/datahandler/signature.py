# -*- coding: utf-8 -*-
"""
Chart dataset

This class makes charts about a dataset:

* spectral signature
* temporal signature

Example:
    >>> from maplearn.datahandler.loader import Loader
    >>> from maplearn.datahandler.signature import Signature
    >>> ldr = Loader('iris')
    >>> sig = Signature()
    >>> sig.plot(ldr.X, title='test')
"""
from __future__ import unicode_literals
import logging

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

#from matplotlib.ticker import MaxNLocator
#from matplotlib.dates import date2num, DateFormatter
# Turn interactive plotting off
plt.ioff()
logger = logging.getLogger('maplearn.' + __name__)

COLOR = "darkgrey"#"#BDE96B"#"#424242"#"#BDE96B"#"#E9D66B"#"#F7D358"
LIM_NFEAT = 15  # number of features that can be displayed at once


class Signature(object):
    """
    Makes charts about a dataset:
    
    * one global graph
    * one graph per class in samples (if samples are available)

    Args:

        * data (array or DataFrame): data to plot
        * dates (list): list of dates (if temporal dataset)
    """

    def __init__(self, data, features=None, dates=None):
        self.dates = dates
        if not isinstance(data, pd.DataFrame):
            self.__data = pd.DataFrame(data, columns=features)
            #data = pd.DataFrame(data.stack())
            #data.reset_index(1, inplace=True)
            #data.columns = ['feature', 'value']


    def plot(self, out_file=None):
        """
        Plots (spectral) signature of data as boxplots or points depending of
        the number of features
        
        Args:

            * data (array or DataFrame): data to plot
            * features (list): list of features inside the dataset
            * out_file (str): name of the output file
        """
        logger.debug('Creating chart...')
        sns.set(style="whitegrid")

        title = 'Signature spectrale du jeu de donnees\n'
        if self.__data.shape[1] > LIM_NFEAT:
            self.__plot(title=title)
        else:
            self.__boxplot(title=title)

        if out_file is not None:
            self.__save(out_file)

    def __plot(self, data=None, title=''):
        """
        Plots (spectral) signature of data as points with confidence interval
        
        Args:

            * title (str): title to add to the plot 
        """        
        if data is None:
            data = self.__data
        else:
            if not isinstance(data, pd.DataFrame):
                data = pd.DataFrame(data)

        g = sns.factorplot(data=data, capsize=.1, linestyles='--',
                           kind='point', color=COLOR, size=5, aspect=2)
        sns.plt.title(title)
        return g

    def __boxplot(self, data=None, title=''):
        """
        Plots (spectral) signature of data as boxplot
        
        Args:

            * title (str): title to add to the plot
        """
        if data is None:
            data = self.__data
        sns.boxplot(data=self.__data, color=COLOR, notch=True)
        sns.plt.title(title)

    def __color_box(self, boxplot, zorder, alpha):
        """
        Colours boxes in the boxplot

        Args:
            * boxplot: plot onto put some colours
            * zorder?
            * alpha: transparency setting
        """
        for box in boxplot['boxes']:
            # change outline color & fill color
            box.set(color='darkgrey', linewidth=2, facecolor='lightgrey',
                    zorder=zorder + 1, alpha=alpha)

        # change color and linewidth of the whiskers
        for whisker in boxplot['whiskers']:
            whisker.set(color='darkgrey', linewidth=2, linestyle='-',
                        zorder=zorder + 2, alpha=alpha)

        # change color and linewidth of the caps
        for cap in boxplot['caps']:
            cap.set(color='darkgrey', linewidth=2, linestyle='-',
                    zorder=zorder + 3, alpha=alpha)

        # change color and linewidth of the medians
        for median in boxplot['medians']:
            median.set(color='darkgrey', linewidth=3, linestyle='-',
                       zorder=zorder + 4, alpha=alpha)

        # fliers <=> outliers
        plt.setp(boxplot['fliers'], color='white', marker='o', markersize=5.0,
                 zorder=zorder + 5, alpha=alpha)

    def plot_class(self, data_classe, title='', out_file=None):

        if not isinstance(data_classe, pd.DataFrame):
                data_classe = pd.DataFrame(data_classe, 
                                           columns=self.__data.columns)
                data_classe = pd.DataFrame(data_classe.stack())
                data_classe.reset_index(1, inplace=True)
                data_classe.columns = ['feature', 'value']
                data_classe['source'] = 'samples'
        
        data = pd.DataFrame(self.__data.stack())
        data.reset_index(1, inplace=True)
        data.columns = ['feature', 'value']
        data['source'] = 'data'
        data = pd.concat([data, data_classe])
        sns.factorplot(data=data, hue='source', x='feature', y='value',
                       capsize=.1, linestyles='--', kind='point', size=5, 
                       aspect=2, legend_out=True,
                       palette= {'data':'lightgrey', 'samples':'darkgrey'})
        sns.plt.title(title)
        if out_file is not None:
            self.__save(out_file)
        
        #plt.close(fig)
        #fig = None

    def __save(self, out_file):
        """
        Save the plot into a file
        
        Args:
            
            * out_file (str): path to the output file
        """
        logger.debug('Trying to save the plot')
        try:
            plt.savefig(out_file, format='png', bbox_inches='tight')
        except IOError as e:
            logger.error("Missing folder : %s", out_file)
            raise IOError(e.message)
        else:
            logger.info('Chart saved in %s', out_file)
        

    def boxplot_classe(self, data, data_classe, title='', features=None,
                       out_file=None):
        """
        Creates a boxplot for the whole dataset and adds boxplot corresponding
        to one class to show signatures

        Args:
            * data: data to chart
            * data_class: data of one class
            * title (str): title of the boxplot
            * features (list): list of features to plot
            * out_file (str): path to the file to save the chart in
        """
        logger.debug('Initializing chart...')
        sns.set(style="white")
        fig = plt.figure(figsize=(7, 5))
        ax0 = fig.add_subplot(1, 1, 1)
        ax0.set_title(title)

        positions = [i for i in range(1, len(features)+1)]

        #sns.boxplot(data=data, x=features, hue='samples', color=COLOR, notch=True,
        #                 whis=1.3)
        #                 positions=[i - .1 for i in positions])
        bp = plt.boxplot(data, notch=1, patch_artist=True, vert=1,
                         whis=1.3, positions=[i - .1 for i in positions])

        self.__color_box(bp, zorder=0, alpha=0.5)

        plt.hold(True)  # superposition d'un second graphe
        bp = plt.boxplot(data_classe, notch=1, patch_artist=True, vert=1,
                         whis=1.3)
        #positions=[i for i in positions]
        self.__color_box(bp, zorder=6, alpha=1)

        if out_file is not None:
            self.__save(out_file)

        # plt.show()
        plt.close(fig)
        fig = None
