# -*- coding: utf-8 -*-
"""
Data handlers: interim classes between file(s) and dataset

* loader: loads data from a file or known datasets
* packdata: creates a dataset with samples and data
* writer: writes data into a file
* signature: graphs a dataset
* echantillons: class about samples

"""