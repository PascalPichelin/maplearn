# -*- coding: utf-8 -*-
"""
Mapping Learning is an application to help automatised cartography (using 
remote sensing images for example).


**Functionnalities**

* many algorithms to classify, cluster or apply regression on your dataset
* research of optimized parameters
* generalize use of robust methods like k-fold
* several preprocessing tasks available : reduction of dimensions...
* reads/writes many file formats, geographic or not
* a standardized report is given with synthetized results
* statiscal and more empirical advices will help novice users

**Development**

Mapping Learning is written in Python and used major Open Source libraries, 
like scikit-learn (Machine Learning algorithms) and osgeo (Gdal/Ogr) to handle
geographic data.

Mapping Learning is a free software, under LGPL license.

**Modules**

Mapping Learning consists of 4 modules:

* app: application modules (configuration, ...)
* ml: machine learning stuff
* filehandler: classes to read/write data from/to a file
* datahandler: classes between a dataset usable in machine learning and the
               usable files


Alban Thomas (alban.thomas@univ-rennes2.fr)
"""

import os
import inspect
import logging
import logging.config


__version__ = '0.3.1'

# application path
dir_app = os.path.dirname(os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe()))))

# logger configuration
if 'logger' not in dir():
    cfg_file = os.path.join(dir_app, 'maplearn', 'logging.cfg')
    if not os.path.exists(cfg_file):
        raise IOError('Given config file for logger is missing: %s' % cfg_file)
    
    logging.config.fileConfig(cfg_file, disable_existing_loggers=True)
    logger = logging.getLogger('maplearn.' + __name__)
