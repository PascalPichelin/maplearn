# -*- coding: utf-8 -*-
"""
Mapping Learning : examples

This script is a good way to discover Mapping Learning possibilities. Using
configuration files available in "examples" subfolder and datasets included in
"datasets" sub-folder, you will see what can do this application.

Example:

        * Asks the user to choose which examples(s) he wants to test::

            $ python run_example.py

        * Execute 3rd example (CLI way)::

            $ python run_example.py 3

        * Launch every examples::

            $ python run_example.py all

"""
from __future__ import print_function
import os
import sys
import subprocess
import inspect
from glob import glob

from maplearn.app.config import Config

# application path
DIR_APP = os.path.dirname(os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe()))))
if __name__ == '__main__' and __package__ is None:
    os.sys.path.append(DIR_APP)


def run_example(cfg_file, path):
    """
    Run one of available examples in "examples" folder

    Args:
        * number (str) : identifies the example to run
        * path (str) : path to run.py script (that launches the application)
    """
    if not os.path.exists(cfg_file):
        print('Configuration file %s is missing => Give up.')
        return None
    subprocess.call(["python",
                     os.path.join(path, "maplearn", "run.py"), "-c", cfg_file])

if __name__ == '__main__':

    # list of available examples
    LST_EX = glob(os.path.join(DIR_APP, 'examples', 'example*.cfg'))
    LST_EX.sort()
    NB_EX = [i.split('examples/example')[-1].split('.cfg')[0] for i in LST_EX]

    if len(sys.argv) > 1:
        if 'all' in sys.argv:
            lst_run = NB_EX
        else:
            lst_run = sys.argv[1:]
        for i in lst_run:
            run_example(LST_EX[NB_EX.index(i)], DIR_APP)
    elif len(sys.argv) == 1:
        for i in LST_EX:
            print(Config(i))
        MSG = 'Choisissez un exemple (%s-%s) :' % (NB_EX[0], NB_EX[-1])
        #PYTHON2/3 comptability
        try:
            reponse = raw_input(MSG).strip()
        except NameError:
            reponse = input(MSG).strip()
        again = True
        while again:
            again = False
            if reponse == 'all':
                for i in LST_EX:
                    run_example(i, DIR_APP)
            if reponse == 'q':
                sys.exit(0)
            elif reponse in NB_EX:
                run_example(LST_EX[NB_EX.index(reponse)], DIR_APP)
            else:
                again = True
                #PYTHON2/3 comptability
                try:
                    reponse = raw_input(MSG).strip()
                except NameError:
                    reponse = input(MSG).strip()
