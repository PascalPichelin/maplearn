# -*- coding: utf-8 -*-
"""
Main class

This class is the engine powering Mapping Learning. It uses every other classes
to load data, apply preprocesses and finally process the dataset, using several
algorithms. The results are synthetized and compared.

The class can apply classification, clustering and regression processes.

Examples:
    >>> from maplearn.app.main import Main

    * Apply 2 different classifications on a known dataset
    
    >>> ben = Main('.', type='classification', algorithm=['knn', 'lda'])
    >>> ben.load('iris')
    >>> ben.preprocess()
    >>> ben.process(True)

    * Apply every available clustering algorithms on the same dataset
    
    >>> ben = Main('.', type='clustering')
    >>> ben.load('iris')
    >>> ben.preprocess()
    >>> ben.process(False) # do not predict results
    
    * Apply regression on another known dataset

    >>> ben = Main('.', type='regression', algorithm='lm')
    >>> ben.load('boston')
    >>> ben.preprocess()
    >>> ben.process(False) # do not predict results

"""
from __future__ import print_function
from __future__ import unicode_literals

import logging
import os
import numpy as np
import pandas as pd

from maplearn.ml.classification import Classification
from maplearn.ml.clustering import Clustering
from maplearn.ml.regression import Regression
from maplearn.datahandler.loader import Loader
from maplearn.datahandler.packdata import PackData
from maplearn.datahandler.writer import Writer

logger = logging.getLogger('maplearn.' + __name__)

class Main(object):
    """
    Realizes every steps from loading dataset to processing

    Args:
        * dossier (str): output path where will be stored every results
        * **kwargs: parameters data and processing to apply on it

    Attributes:
        * dataset (PackData): dataset to play with
    """
    __algorithms = None
    __dctCodes = None

    def __init__(self, dossier, **kwargs):
        self.__machine = None
        self.__io = {'dossier': dossier, 'features': None}  # dossier en sortie
        if 'na' in kwargs:
            self.__io['na'] = kwargs['na']
        else:
            self.__io['na'] = np.nan
        self.dataset = None
        if 'codes' in kwargs:
            self.__dct_codes = kwargs['codes']
        else:
            self.__dct_codes = None
        self.__metadata = dict(process=kwargs['type'])
        self.__machine = eval(kwargs['type'], {"__builtins__": None},
                         {"classification": Classification,
                         "clustering": Clustering,
                         "regression": Regression})(data=None,
                         algorithms=kwargs['algorithm'], iK=2,
                         dirOut=self.__io['dossier'], **kwargs)

        self.__algorithms = self.__machine.algorithms

    def load(self, source, **kwargs):
        """
        Loads samples (labels with associated features) used for training
        algorithms

        Args:
            * source (str): file to load or name of an available datasets
            * **kwargs: parameters to specify how to use datasets (which
                        features to use...)
        """
        print('##1. Chargement des donnees ##\n')
        for i in ['label', 'label_id', 'features']:
            if i not in kwargs:
                kwargs[i] = None
        loading = Loader(source, label_id=kwargs['label_id'],
                         label=kwargs['label'],
                         features=kwargs['features'],
                         codes=self.__dct_codes)
        print(loading)
        source = os.path.basename(os.path.splitext(source)[0])
        self.__io['source'] = loading.src
        if self.__dct_codes is not None:
            codes = self.__dct_codes
        else:
            codes = loading.nomenclature

        if kwargs['features'] is None:
            kwargs['features'] = loading.features

        self.dataset = PackData(loading.X, loading.Y, data=loading.aData,
                                codes=codes, source=source,
                                features=kwargs['features'],
                                type=self.__metadata['process'],
                                na=self.__io['na'],
                                dirOut=self.__io['dossier'])

        if 'source' in self.__io and self.__io['source']['type'] != 'ImageGeo':
            self.__io['features'] = kwargs['features']

    def load_data(self, source, label_id=None, label=None, features=None):
        """
        Load dataset to predict with previously trained algorithms

        Args:
            * source (str): path to load or name of an available dataset
            * label_id (optional[str]): column used to identify labels
            * label (optional[str]): column with labels' names
            * features (list): columns to use as features. Every available
                             columns are used if None
        """
        if not features is None and not self.__io['features'] is None:
            # selection des features a utiliser (dans jeu de données)
            features = [i for i in features if i in self.__io['features']]
        elif not self.__io['features'] is None:
            features = self.__io['features']

        loading = Loader(source, label_id=label_id, label=label,
                         features=features, codes=self.__dct_codes)
        print(loading)
        if features is None:
            features = loading.features

        if 'source' in self.__io and self.__io['source']['type'] == 'ImageGeo':
            y = np.copy(self.dataset.data)
            x = np.copy(loading.matrix[y != self.__io['na']])
            y = y[y != self.__io['na']]
            self.dataset = PackData(x, y, data=loading.aData,
                                    type=self.__metadata['process'],
                                    source=source, features=features)
        else:
            self.dataset.data = loading.aData
        self.__io['data'] = loading.src

    def preprocess(self, **kwargs):
        """
        Apply preprocessings tasks asked by user and give the dataset to the
        Machine Learning processor

        Args:
            * **kargs: available preprocessing tasks (scaling dataset, reducing
                     number of features...)
        """
        print('##2. Pretraitement ##\n')
        b_plot = False
        # rééchantillonne les échantillons
        if 'balance' in kwargs:
            if kwargs['balance']:
                self.dataset.balance()
                b_plot = True

        # Normalisation
        if 'scale' in kwargs:
            if kwargs['scale']:
                self.dataset.scale()
                b_plot = True

        # Reduction du nombre de dimensions du jeu de données
        if 'reduce' in kwargs and kwargs['reduce'] is not None:
            if not 'ncomp' in kwargs:
                kwargs['ncomp'] = None
            else:
                if kwargs['reduce'] != 'refcv':
                    self.dataset.reduit(meth=kwargs['reduce'],
                                        ncomp=kwargs['ncomp'])
            b_plot = True

        # analyse de separabilite
        if 'separability' in kwargs:
            if kwargs['separability']:
                if 'metric' in kwargs:
                    metric = kwargs['metric']
                else:
                    metric = 'euclidean'
                try:
                    self.dataset.separability(metric=metric)
                except ValueError:
                    logger.error("Separability can't be analysed")

        # mise a dispo des donnees pour traitement
        if self.__machine.load(data=self.dataset) != 0:
            raise IOError("Error when loading data")

        if 'reduce' in kwargs and kwargs['reduce'] == 'refcv':
            self.__machine.rfe(None)

        if b_plot:
            self.dataset.plot(os.path.join(self.__io['dossier'],
                                           'sig2_data.png'))
            print(self.dataset)

    def process(self, predict=False, optimize=False):
        """
        Apply algorithms to dataset

        Args:
            * predict (bool): should the algorithms be only fitted on 
                            samples or also predict results ?
            * optimize (bool): should Mapplng Learning look for optimized
                                 parameters on algorithms ?

        """
        print('##3. Traitement ##\n')
        # SELECTION DES FEATURES par RFE
        # self.__machine.rfe(self.__algorithms)

        # optimization of algorithms (look for best parameters)
        if optimize:
            # optimized algorithms are added to algorithms list
            for algo in self.__algorithms:
                self.__machine.optimize(algo)

        # compares algorithm predictions
        self.__machine.run(predict)
        
        if predict:
            result = pd.DataFrame(data=np.zeros((len(self.dataset.not_nas),
                                                 len(self.__machine.result.columns))),
                                                columns=self.__machine.result.columns)
            result.ix[self.dataset.not_nas] = self.__machine.result.as_matrix()

            # ecriture des resultats dans un fichier
            if self.__io['data']['type'] in ['ImageGeo', 'Shapefile']:
                w = Writer(self.__io['data']['path'],
                           origin=self.__io['data']['path'])
            else:
                w = Writer(self.__io['data']['path'])
            ext = os.path.splitext(self.__io['data']['path'])[-1]
            out_file = os.path.join(self.__io['dossier'], 'export' + ext)
            # if self.__io['data']['type'] == 'ImageGeo':

            w.run(data=result, path=out_file)
        logger.info('Mapping Learning: end of processes')
        print('<p class="signature">powered by <a href="https://bitbucket.org/thomas_a/maplearn/">Mapping Learning</a></p></body>')

    def __del__(self):
        self.__machine = None
        self.dataset = None
        self.__metadata= None
        self.__io = None