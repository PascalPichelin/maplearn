#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Mapping Learning Configuration


This class configuration Mapping Learning based on a configuration file. It is
also able to re-write a re-usable configuration file based on some attributes.

"""
from __future__ import print_function
from __future__ import unicode_literals

import os
import re
from datetime import datetime
import logging

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

logger = logging.getLogger('maplearn.' + __name__)


def splitter(text):
    """
    Splits a character string based on several separators and remove useless
    empty characters.

    Args:
        text (str) : character string to split

    Returns:
        list: list of stripped character strings, None elsewhere
    """
    if isinstance(text, list):
        return text
    try:
        text = re.split("[,;|]", text)
    except TypeError:
        return None
    text = [i.strip() for i in text]
    return text


class Config(object):
    """
    This class is the medium between a configuration file and the applicaton.
    It is able to load and check a configuration file (Yaml) and rewrite a new
    configuration file (that can be re-used by Mappling Learning later).

    Args:
        config_file (str) : path to a configuration file

    The class attributes described below reflects the sections in configuration
    file.

    Attributes:

        * io (dict): input/output. path to samples, dataset files and output.
                   list of features to use...
        * codes (dict): label codes and corresponding names
        * preprocess (dict) : which preprocessing step(s) to apply
        * process (dict) : which processes to apply (list of algorihms...)
    """
    __METADATA = dict(
            preprocess={'scale': 'Scale',
                        'balance': 'Reequilibrage des echantillons',
                        'separability': 'Analyse de separabilite des \
                         echantillons'})

    def __init__(self, file_config):
        self.__data = dict(
            metadata={'title':"Mapping Learning's report", 'description':None,
                  'author':None, 'date':datetime.today().strftime('%d/%m/%Y')},
            io={'data':None, 'samples':None, 'features':None,
                'label':None, 'label_id':None, 'na':None,
                'output': os.path.join(os.getcwd(), 'tmp')},
            codes=None,
            preprocess={'scale': True, 'balance': False, 'reduce': None,
                        'ncomp': None, 'separability': False},
            process={'type': None, 'kfold': 3, 'optimize':False,
                     'algorithm':None,
                     'predict':False, 'n_clusters': 15, 'distance':'euclidean'}
            )

        if file_config is not None and not os.path.exists(file_config):
            str_msg = "Configuration file is missing (%s)" % file_config
            logger.critical(str_msg)
            raise configparser.Error(str_msg)
        logger.debug('Configuration file : %s', file_config)
        self._file_cfg = file_config
        self.__cfg = configparser.ConfigParser()
        self.__cfg._boolean_states = {'t': True, 'true': True,
                                      'f': False, 'false':False, 'oui':True,
                                      'non': False, 'o':True, 'n':False}
        if self._file_cfg is not None:
            self.read()

    def check(self):
        """
        Check that parameters stored in attributes are correct

        Returns:
            int : number of issues detected
        """
        nb_pbs = 0

        if not os.path.exists(self.__data['io']['output']):
            os.makedirs(self.__data['io']['output'])

        # liste des source(s) en entree
        self.__data['io']['features'] = splitter(self.__data['io']['features'])
        if self.__data['io']['samples'] is not None:
            if self.__data['io']['samples'] in ['iris', 'digits', 'boston']:
                logger.debug('Choosen samples : %s', self.__data['io']['samples'])
            elif not os.path.exists(self.__data['io']['samples']):
                logger.critical("Missing samples source file (%s)",
                                self.__data['io']['samples'])
                nb_pbs += 1

        if self.__data['io']['data'] is not None:
            if not os.path.exists(self.__data['io']['data']):
                logger.critical("Missing data source file (%s)",
                                self.__data['io']['data'])
                nb_pbs += 1

        logger.info('Configuration checked : %i issue(s)', nb_pbs)

        return nb_pbs

    def __set_str(self, section, *params):
        """
        Sets several parameters
        """
        for i in params:
            try:
                param = self.__cfg.get(section, i).strip()
            except configparser.NoOptionError:
                logger.debug('Parameter %s (%s) poorly or not defined', i,
                               section)
            else:
                if param != '' and param is not None:
                    self.__data[section][i] = param
                    logger.debug('Parameter %s (%s) set', i, section)
                else:
                    logger.debug('Parameter %s (%s) not set', i, section)

    def __set_bool(self, section, *params):
        for i in params:
            try:
                self.__data[section][i] = self.__cfg.getboolean(section, i)
            except configparser.NoOptionError:
                logger.debug('Parameter %s (%s) not defined', i, section)
            except ValueError:
                logger.debug('Parameter %s (%s) poorly defined', i, section)
            else:
                logger.debug('Parameter %s (%s) set', i, section)

    def __set_int(self, section, *params):
        for i in params:
            try:
                self.__data[section][i] = self.__cfg.getint(section, i)
            except configparser.NoOptionError:
                logger.debug('Parameter %s (%s) not defined', i, section)
            except ValueError:
                logger.debug('Parameter %s (%s) poorly defined', i, section)
            else:
                logger.debug('Parameter %s (%s) set', i, section)

    def read(self):
        """
        Load parameters from configuration file and put them in corresponding
        class attributes

        Returns:
            int : number of issues got when reading the file
        """
        logger.debug('Trying to read configuration...')
        self.__cfg.read(self._file_cfg)

        if self.__cfg.has_section('metadata'):
            self.__set_str('metadata', 'title', 'description', 'author')

        # Defines input/output
        self.__set_str('io', 'samples', 'label_id', 'label', 'features',
                       'data', 'na', 'output')
        if self.__data['io']['output'] is not None:
            self.__data['io']['output'] = os.path.normpath(self.__cfg.get('io',
                                                                 'output'))

        # get legend of labels if given in configuration file
        if self.__cfg.has_section('codes'):
            self.__data['codes'] = dict(self.__cfg.items('codes'))
            for i in self.__data['codes']:
                self.__data['codes'][int(i)] = self.__data['codes'].pop(i)
            logger.info('Legend read with %i classes',
                        len(self.__data['codes']))

        # Pretraitements
        self.__set_str('preprocess', 'reduce')
        self.__set_int('preprocess', 'ncomp')
        self.__set_bool('preprocess', 'scale', 'separability', 'balance')

        # Traitements
        self.__set_str('process', 'type', 'distance', 'algorithm')
        self.__set_bool('process', 'optimize', 'predict')
        self.__set_int('process', 'kfold', 'n_clusters')
        # list of algorithm(s)
        self.__data['process']['algorithm'] = splitter(self.__data['process']['algorithm'])

        # Check configuration
        nb_pbs = self.check()
        if nb_pbs == 0:
            logger.info('Configuration file read !')
        else:
            logger.critical('%i issue(s) when loading configuration file',
                            nb_pbs)
            raise configparser.Error('Configuration error(s)')
        return nb_pbs

    @property
    def io(self):
        """
        Input/Output property
        """
        return self.__data['io']

    @property
    def codes(self):
        """
        Dictionnary describing label codes and the name of classes
        """
        return self.__data['codes']

    @property
    def preprocess(self):
        """
        Dictionnary of preprocess parameters
        """
        return self.__data['preprocess']

    @property
    def process(self):
        """
        Dictionnary of process parameters
        """
        return self.__data['process']

    def write(self, fichier):
        """
        Write a new configuration file feeded by class attributes content.

        Args:
            fichier (str) : path to configuration file to write
        """
        __cfg = configparser.ConfigParser()
        for i in self.__data.keys():
            if self.__data[i] is not None:
                __cfg.add_section(i)
                for j in self.__data[i].keys():
                    if self.__data[i][j] is None:
                        value = ''
                    elif isinstance(self.__data[i][j], list):
                        value = ','.join(self.__data[i][j])
                    else:
                        value = self.__data[i][j]
                    try:
                        value = str(value)
                    except AttributeError:
                        pass
                    __cfg.set(i, str(j), value)
        with open(fichier, 'w') as __file:
            __cfg.write(__file)

    def __str__(self):

        str_msg = '<a href="https://bitbucket.org/thomas_a/maplearn">![logo]'
        str_msg += u'(%s){: .logo}</a> \n\n#%s#\n' % (os.path.join('img', 'logo.png'), self.__data['metadata']['title'])
        if self.__data['metadata']['description'] is not None:
            str_msg += u'\n\n##Description##\n%s\n' % self.__data['metadata']['description']
        str_msg += u'\n\n####Realisation : '
        if self.__data['metadata']['author'] is not None:
            str_msg += '%s - ' % self.__data['metadata']['author']
        str_msg += '%s####\n' % self.__data['metadata']['date']
        str_msg += '\n\n##Resume##'
        str_msg += '\n* Entree(s)\n'
        if not self.__data['io']['samples'] is None:
            str_msg += '\n\t- Echantillons : %s' % self.__data['io']['samples']
        if not self.__data['io']['data'] is None:
            str_msg += '\n\t- Donnees : %s' % self.__data['io']['data']
        str_msg += '\n\n* Pretraitement'
        for i in ['scale', 'balance', 'separability']:
            if self.__data['preprocess'][i]:
                str_msg += '\n\t- %s' % self.__METADATA['preprocess'][i]
        if self.__data['preprocess']['reduce'] is not None:
            str_msg += '\n\t- Reduction des dimensions par %s' \
                      % self.__data['preprocess']['reduce']

        str_msg += '\n* Traitement : %s' % self.__data['process']['type']
        if self.__data['process']['algorithm'] is None:
            str_msg += '\n\t- Algorithmes : Tous'
        else:
            str_msg += '\n\t- %i algorithme(s) : %s' \
                   % (len(self.__data['process']['algorithm']),
                   ', '.join(self.__data['process']['algorithm']))
        if self.__data['process']['type'] == 'clustering' and \
            self.__data['process']['n_clusters'] is not None:
            str_msg += '\n\t- %i cluster(s) souhaites' % self.__data['process']['n_clusters']
        if self.__data['process']['optimize']:
            str_msg += '\n\t- Optimisation des algorithmes'
        str_msg += '\n\t- Approche %i-fold' % self.__data['process']['kfold']
        str_msg += '\n\t- Distance : %s' % self.__data['process']['distance']
        str_msg += '\n* Sortie\n\t- Dossier : %s' % self.__data['io']['output']
        return str_msg

    def __del__(self):
        self.__cfg = None
        self.__data = None