.. Mapping Learning documentation master file, created by
   sphinx-quickstart on Sat Oct 29 19:10:23 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mapping Learning's documentation!
============================================

Contents:

.. toctree::
   :maxdepth: 2
.. automodule:: maplearn
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

