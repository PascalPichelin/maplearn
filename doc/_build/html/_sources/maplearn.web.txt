maplearn.web package
====================

.. automodule:: maplearn.web
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    maplearn.web.djmaplearn
    maplearn.web.mlweb

Submodules
----------

maplearn.web.manage module
--------------------------

.. automodule:: maplearn.web.manage
    :members:
    :undoc-members:
    :show-inheritance:


