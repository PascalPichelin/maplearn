maplearn.test package
=====================

Submodules
----------

maplearn.test.test_classification module
----------------------------------------

.. automodule:: maplearn.test.test_classification
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_clustering module
------------------------------------

.. automodule:: maplearn.test.test_clustering
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_config module
--------------------------------

.. automodule:: maplearn.test.test_config
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_confusion module
-----------------------------------

.. automodule:: maplearn.test.test_confusion
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_distance module
----------------------------------

.. automodule:: maplearn.test.test_distance
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_echantillon module
-------------------------------------

.. automodule:: maplearn.test.test_echantillon
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_excel module
-------------------------------

.. automodule:: maplearn.test.test_excel
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_filehandler module
-------------------------------------

.. automodule:: maplearn.test.test_filehandler
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_imagegeo module
----------------------------------

.. automodule:: maplearn.test.test_imagegeo
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_loader module
--------------------------------

.. automodule:: maplearn.test.test_loader
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_main module
------------------------------

.. automodule:: maplearn.test.test_main
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_packdata module
----------------------------------

.. automodule:: maplearn.test.test_packdata
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_reduction module
-----------------------------------

.. automodule:: maplearn.test.test_reduction
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_regression module
------------------------------------

.. automodule:: maplearn.test.test_regression
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_shapefile module
-----------------------------------

.. automodule:: maplearn.test.test_shapefile
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_signature module
-----------------------------------

.. automodule:: maplearn.test.test_signature
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.test.test_writer module
--------------------------------

.. automodule:: maplearn.test.test_writer
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: maplearn.test
    :members:
    :undoc-members:
    :show-inheritance:
