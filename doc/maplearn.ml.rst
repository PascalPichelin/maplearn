maplearn.ml package
===================

.. automodule:: maplearn.ml
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

maplearn.ml.classification module
---------------------------------

.. automodule:: maplearn.ml.classification
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.ml.clustering module
-----------------------------

.. automodule:: maplearn.ml.clustering
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.ml.confusion module
----------------------------

.. automodule:: maplearn.ml.confusion
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.ml.distance module
---------------------------

.. automodule:: maplearn.ml.distance
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.ml.machine module
--------------------------

.. automodule:: maplearn.ml.machine
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.ml.reduction module
----------------------------

.. automodule:: maplearn.ml.reduction
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.ml.regression module
-----------------------------

.. automodule:: maplearn.ml.regression
    :members:
    :undoc-members:
    :show-inheritance:


