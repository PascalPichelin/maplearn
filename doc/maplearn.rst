maplearn package
================

.. automodule:: maplearn
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    maplearn.app
    maplearn.datahandler
    maplearn.filehandler
    maplearn.ml
    maplearn.web

Submodules
----------

maplearn.run module
-------------------

.. automodule:: maplearn.run
    :members:
    :undoc-members:
    :show-inheritance:

maplearn.run_example module
---------------------------

.. automodule:: maplearn.run_example
    :members:
    :undoc-members:
    :show-inheritance:


